package com.example.estructurascontrol.repeticion;

/**
 * Created by IntelliJ IDEA.
 * User: QSP
 * Date: 03/10/22
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */
public class For {

    public static void main(String[] args){

        for(int i = 0; i < 10; i++) {
            System.out.println("El valor de i es: " + i );
        }
        
        String[] nombres = {"Pepe", "Janito", "Roberta"};
        for(int i = 0; i < nombres.length; i++){
            System.out.println(nombres[i]);
        }

        int suma = 0;
        int[] numeros = {5, 7, 8};
        for(int i = 0; i < numeros.length; i++){
            System.out.println(numeros[i]);
            suma = suma + numeros[i];
            suma += numeros[i];
        }

        System.out.println(suma);
    }
}
