package com.example.estructurascontrol.repeticion;

/**
 * Crear un bucle que permita concatenar texto e imprima el resultado por consola (deben venir de un array)
 */

import java.util.Scanner;

public class Ejercicio {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Cuantas palabras desea ingresar?");

        int n = 0;

        n = scanner.nextInt();

        System.out.println("Por favor ingrese la/s " + n + " palabras de su eleccion. Pulse entrar luego de cada " +
                "palabra (se armara un texto en base a ellas)");

        String[] palabras = new String[n + 1];


        for(int i = 0; i < palabras.length; i++){

            palabras[i] = scanner.nextLine();
        }

        //Ejemplo de devolucion con funcion con return y void
        String palabrasConcatenadas = concatenadorFor(palabras);
            System.out.println("Este es el texto que se armo con sus palabras ingresadas " + palabrasConcatenadas);

        concatenadorForEach(palabras);
        concatenadorWhile(n, palabras);


        String palabra = "";
        for(int i = 0; i < palabras.length; i++) {

            palabra = palabra.concat(palabras[i] + " ");
            
        }
        
        System.out.println(palabra);
        
    }

        // Con for normal
        static String concatenadorFor(String[] words){

            String palabrasConcatenadasFor = "";


            for(int i = 0; i < words.length; i++){

                palabrasConcatenadasFor += words[i] + " ";

            }

            return palabrasConcatenadasFor;
        }


        //con for each
        static void concatenadorForEach(String[] words){

            String palabrasConcatenadasForEach = "";

            for(String palabra : words){

                palabrasConcatenadasForEach += palabra + " ";

            }

            System.out.println("Este es el texto que se armo con sus palabras ingresadas " + palabrasConcatenadasForEach);
        }

        //Con while
        static void concatenadorWhile(int cantidadDePalabras, String[] words ){

            int contador = 0;
            String palabrasConcatenadasWhile = "";

            while(contador <= cantidadDePalabras){

                palabrasConcatenadasWhile += words[contador] + " ";
                contador++;

            }

            System.out.println("Este es el texto que se armo con sus palabras ingresadas " + palabrasConcatenadasWhile);


        }


}