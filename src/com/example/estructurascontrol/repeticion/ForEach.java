package com.example.estructurascontrol.repeticion;

/**
 * Created by IntelliJ IDEA.
 * User: QSP
 * Date: 04/10/22
 * Time: 15:28
 * To change this template use File | Settings | File Templates.
 */
public class ForEach {
    
    public static void main(String[] args){

        String[] nombres = {"Pepe", "Janito", "Roberta"};
        
        for(String nombre : nombres){
            System.out.println(nombre);
        }

        int suma = 0;
        int[] numeros = {5, 10, 15};
        for(int numero : numeros){
            suma += numero;
        }

            System.out.println(suma);

    }

}
