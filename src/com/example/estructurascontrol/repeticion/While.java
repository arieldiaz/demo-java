package com.example.estructurascontrol.repeticion;

/**
 * Created by IntelliJ IDEA.
 * User: QSP
 * Date: 04/10/22
 * Time: 15:39
 * To change this template use File | Settings | File Templates.
 */
public class While {
    
    public static void main(String[] args){

        int i = 0;
        String[] nombres = {"Pepe", "Janito", "Roberta"};

        while(i < nombres.length){
            System.out.println(nombres[i]);
            i++;
        }
        
        int contador = 0;
        
        while(contador < 10){
            // String nombre = "Prueba"; las variables creadas dentro del bucle solo tienenvalor dentro del mismo, no afuera
            contador++;

            if(contador == 5){
               //break;
                continue;
            }

            System.out.println("Valor de contador " + contador);

        }

    }
}
