package com.example.estructurascontrol.condicionales;

/**
 * Created by IntelliJ IDEA.
 * User: QSP
 * Date: 03/10/22
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
public class IfElseIf {

    public static void main(String[] args) {

        String dia = "Viernes";

        boolean resultado = dia.equals("Martes");

        if (dia.equals("Lunes")) {
            System.out.println("Animo con la semana");

        } else if (dia.equals("Martes")) {
            System.out.println("Hoy es Martes");

        } else if (dia.equals("Miercoles")) {
            System.out.println("Miercoles con Mi de Mitad de semana");

        } else {
            System.out.println("Hoy es Viernes, llegamos!!!!");
        }
    }
}