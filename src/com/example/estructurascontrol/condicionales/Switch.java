package com.example.estructurascontrol.condicionales;

/**
 * Created by IntelliJ IDEA.
 * User: QSP
 * Date: 03/10/22
 * Time: 16:29
 * To change this template use File | Settings | File Templates.
 */
/*
public class Switch {
    
    public static void main(String[] args){

        String dia = "Viernes";

        switch(dia){
            case "Lunes":
                System.out.println("Animo con la semana");
                break;

            case "Martes":
                System.out.println("Hoy es Martes");
                break;

            case "Miercoles":
                System.out.println("Miercoles con Mi de Mitad de semana");
                break;

            case "Jueves":
                System.out.println("Jueves,.... ya casi!");
                break;

            case "Viernes":
                System.out.println("Hoy es Viernes, llegamos!!!!");
                break;

            case "Sabado":
                System.out.println("Sabado");
                break;

            case "Domingo":
                System.out.println("Domingo, depresion....");
                break;

            default:
                System.out.println("No es un dia valido");
                break;
        }

    }
}

/*
* en esta version switch no soporta String
*
*/