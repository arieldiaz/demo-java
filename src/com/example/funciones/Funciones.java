package com.example.funciones;

/**
 * Created by IntelliJ IDEA.
 * User: QSP
 * Date: 02/10/22
 * Time: 08:45
 * To change this template use File | Settings | File Templates.
 */
//se llama funcion cuando no esta asociado a un objeto, y metodo cuando si lo esta
public class Funciones {
    
    public static void main (String[] args) {
        
        showMenu();
        showMenu();
        
        String menu = getMenu();
        System.out.println(menu);
        //o
        System.out.println(getMenu());

        double Price  = getPrice();
        
        //Opcion 3: funcion con parametro y sin tipo de retorno (paso por valor; lo que se modifique del valor dentro de la funcion no lo modifica afuera, trabaja con una copia)
        imprimirSaludo("OpenBootcamp");
        
        //Opcion 4:
        String nombre = "Alan ";
        String apellido = " Sastre";
        String saludo = obtenerSaludo(nombre, apellido);
        System.out.println(saludo);

        int resultado = suma(50, 50);

        System.out.println(resultado);
        
        //Sobrecarga: permite duplicar un metodo siempre y cuando tengan  diferente numero/tipo parametro
        
        
    }

    private static int suma(int numero1, int numero2) {
        return numero1 + numero2;
    }

    private static int suma(int numero1, int numero2, int numero3) {
        return numero1 + numero2 + numero3;
    }

    private static double suma(double numero1, double numero2) {
        return numero1 + numero2;
    }


    static String obtenerSaludo(String nombre, String apellido) {     //no mas de 3 o 4 parametros
        return "Buenas tardes " + nombre + " " + apellido;
    }

    static void imprimirSaludo(String nombre) {
        System.out.println("Buenos dias " + nombre);
    }

    static double getPrice() {
        return 100.99;  //To change body of created methods use File | Settings | File Templates.
    }

    //static; que pertenece a esta clase, no es necesario crear objeto
    static void showMenu(){
        System.out.println("Bienvenidos al E-commerce de zapatillas");
        System.out.println("1 -Ver zapatillas");
        System.out.println("2 - Comprar zapatillas");
        System.out.println("3 - Salir");
    }

    static String getMenu(){
        return "Bienvenidos al E-commerce de zapatillas: \n 1 - Ver zapatillas.... ";

    }
}

